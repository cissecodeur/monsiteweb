import React, { Component } from 'react';
import { Grid , Cell } from 'react-mdl';
import { Etablissements } from './Etablissements';
import { Experiences } from './Experiences';
import { Competences } from './Competences';

export class Presentation extends Component {

    render(){
        return (
            <div>
             <Grid>
                <Cell col = {4}> 
                  <div style = {{ textAlign:"center" }}>
                  <img src = "https://www.shareicon.net/download/2015/09/18/103157_man_512x512.png"
                    alt = "avatar"
                    style = {{ height : "200px"}}
                  />
                  
                 </div>
                 <h2 style = {{ padding:"2em"}}>Cisse Yacouba</h2>
                 <h4 style = {{ color:"grey"}}>Adminstrateur Systemes / Developpeur web</h4>
                 <hr style = {{ borderTop:"3px solid #833fb2", width:"100% "}} />
                 <p>
                         Je suis un jeune informaticien ivoirien dynamique,autonome et passionné de nouvelles technologies.<br/>
                        Mes recherches,sont plus axées,sur la programmation notamment Springboot,Python
                        et aussi reactjs comme framework front end.<br/>
                        Aussi,je m'interesse a l'administration et la securité des serveurs mais egalement a la culture
                        DevOps notamment les technos comme Docker,Ansible,Jenkins que j'integres quotidiennements dans
                        la realisation de mes projets.<br/>
                        Je reste disponible et attentif a toutes les opportunités.<br/>
                        <strong> Bienvenue sur mon blog et bonne lecture.</strong>
                  </p>
                 <hr style = {{ borderTop:"3px solid #833fb2", width:"100% "}} />
                  <h5>Adresse Email</h5>
                  <p>Yacoubc01@gmail.com</p>
                  <h5>Telephone</h5>
                  <p>(+225) 78773960</p>
                  <h5>Adresse Email</h5>
                  <h5>web</h5>
                  <p>https://monsiteweb.com</p>              
                </Cell>
                <Cell className = "presentation-right-col" col = {8}> 
                  <h2>Etablissements</h2>
                  < Etablissements 
                    AnneeDebut = {2015}  
                    AnneeFin = { 2017 }
                    NomEcole = "Groupe Edhec"
                    DescriptionEcole = " Licence 3 et Master Genie Logiciel au Groupe Edhec qui est un etablissement ivoirien formant des ingenieurs  dans tout les domaines."
                  />
                  < Etablissements 
                    AnneeDebut = {2011}  
                    AnneeFin = { 2013 }
                    NomEcole = "Agitel Formation"
                    DescriptionEcole = " BTS Informatique Developpeur d'Applications a Agitel Formation qui est une grande ecole d'excellence formant les ingenieurs dans tout les domaines."
                  />
                  < Etablissements 
                    AnneeDebut = {2008}  
                    AnneeFin = { 2011 }
                    NomEcole = "Lycee Scientifique de Yamoussoukro"
                    DescriptionEcole = " BAC au lycée scientifique de yamoussoukro qui est un etablissement secondaire accueillant les meilleurs eleves de la nation ivoirienne."
                  />
                  <hr style = {{ borderTop : "3px solid #e22947"}}/>
                   <h2>Experiences</h2>
                   < Experiences
                    AnneeDebutJob = { 2019 }
                    AnneeFinJob = "Aujourd'hui"
                    PosteJob = "Administrateur Systemes"
                    NomBoite = "Vename"
                    DescriptionBoite = "Vename est une entreprise ivoirienne d'hebergement de sites web et de ventes de serveurs dediées"
                   />
                   < Experiences
                    AnneeDebutJob = { 2017 }
                    AnneeFinJob = { 2019 }
                    PosteJob = "Developpeur backend springboot"
                    NomBoite = "Afrique Conseils Informatiques et Managements (ACIM)"
                    DescriptionBoite = "ACIM est une Startup Ivoirienne d'integrations de solutions informatiques ."
                  />
                  
                  < Experiences
                    AnneeDebutJob = { 2015 }
                    AnneeFinJob = { 2017 }
                    NomBoite = "Self Employed"
                    DescriptionBoite = " Travaux sur des projets personnels ou avec d'autres developpeurs, dans l'administration et la securité des serveurs Linux et aussi dans la maintenance des ordinateurs."
                  />
                  
                   <hr style = {{ borderTop : "3px solid #e22947"}}/>
                   <h2>Competences</h2>
                   <Competences 
                       Competences = "HTML/CSS"
                       progress = { 90 }
                   />
                   <Competences 
                       Competences = "REACTJS"
                       progress = { 60 }
                   />
                   <Competences 
                       Competences = "SPRINGBOOT"
                       progress = { 75 }
                   />
                   <Competences 
                       Competences = "DOCKER"
                       progress = { 70 }
                   />
                   <Competences 
                       Competences = "ANSIBLE"
                       progress = { 60 }
                   />
                   <Competences 
                       Competences = "WORDPRESS"
                       progress = { 80 }
                   />
                   <Competences 
                       Competences = "JENKINS"
                       progress = { 50 }
                   />
                   <Competences 
                       Competences = "ADMINISTRATION LINUX"
                       progress = { 83 }
                   />

                </Cell>
             </Grid>   
            </div>
            
    
        );
    }
}