import React, { Component } from 'react';

export class Footer extends Component {

    render(){
        return(
            <div>
                <div>
                    <footer className="main-footer">
                     <div className="pull-right hidden-xs">
                        <b>Version</b> 2.4.18
                     </div>
                    <strong>Copyright © 2014-2019 <a href="https://monsite.com">AdminLTE</a>.</strong> All rights
                    reserved.
                    </footer>
                </div>
  
           </div>
);

    }
}