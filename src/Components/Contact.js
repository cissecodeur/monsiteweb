import React, { Component } from 'react';
import { Grid, Cell, List,ListItem,ListItemContent } from 'react-mdl';

export class Contact extends Component {

    render(){
        return (
            <div className = "contact-body">
                 <Grid className = "contact-grid">
                  <Cell col = {6}>
                    {/* 
                    <h2>R4Y</h2>
                    <img 
                       src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTm6wHQXv7NfeK9DHeYuUmT5hYcNMtzytwVL4s8dH-i4wjjZQ9ewA"
                       style = {{ height : "400 px" }}
                       alt = "avatar"
                    />
                    */}

                    <p style = {{ margin : "auto" , width: "100%" ,paddingTop:"10em"}}>
                                   Hello , pour toutes vos questions ou pour la gestion de vos projets,
                                       vous pouvez me contacter sur ces differentes adresses.
                           
                    </p>
                    
                  </Cell> 
                  <Cell col = {6}>
                     <h2>Me contacter</h2>
                       <hr/> 
                       <div className = "contact-list">
                       <List>
                            <ListItem>
                                <ListItemContent style = {{ fontSize:'30px', fontFamily:'Anton' }}>
                                  <i className = "fa fa-phone-square" aria-hidden = "true"/>
                                    (+225) 747-482-87
                                </ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent style = {{ fontSize:'30px', fontFamily:'Anton' }}>
                                  <i className = "fa fa-fax" aria-hidden = "true"/>
                                    (+225) 747-482-87
                                </ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent style = {{ fontSize:'30px', fontFamily:'Anton' }}>
                                  <i className = "fa fa-envelope" aria-hidden = "true"/>
                                      yacoubc01@gmail.com
                                </ListItemContent>
                            </ListItem>
                            <ListItem>
                                <ListItemContent style = {{ fontSize:'30px', fontFamily:'Anton' }}>
                                  <i className = "fa fa-skype" aria-hidden = "true"/>
                                    Mon id Skype : 
                                </ListItemContent>
                            </ListItem>
                            
                                
                            </List>
                        </div>  
                  </Cell>                   
                 </Grid>
            </div>
        );
    }
}