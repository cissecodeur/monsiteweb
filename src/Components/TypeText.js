import React , { Component } from 'react';
import { render } from 'react-dom';
import { Typed } from 'react-typed';


export class TypeText extends Component{

    render(){
        return(
                          
         <div>
             <Typed  
                    strings ={['Je suis M.Cisse Yacouba , je suis Administrateur Systeme , je suis Developpeur Web Full Stack,Je suis egalement un passionné de la culture DevOps...']} 
                    typeSpeed = {40}
                    backSpeed = {60}
                    loop
              /> 
        </div>
         );
    }
}

