import React , { Component } from 'react';
import { Grid , Cell } from 'react-mdl';

export class Experiences extends Component{

    render(){
        return (
            <div>
            <Grid>
              <Cell col = {4}>
                  <p>{ this.props.AnneeDebutJob } - { this.props.AnneeFinJob } </p>
              </Cell>
                  
            
              <Cell col = {8}>
                 <h4 style = {{ marginTop : "0px"}}>{ this.props.NomBoite } </h4>
                 <p>{ this.props.PosteJob }</p>
                 <p>{ this.props.DescriptionBoite }</p>
              </Cell>
            </Grid>
        </div>

        );
    }
}