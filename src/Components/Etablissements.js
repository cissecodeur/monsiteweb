import React,{ Component } from 'react';
import { Cell , Grid } from 'react-mdl';

export class Etablissements extends Component {

    render(){
        return (
            <div>
                <Grid>
                  <Cell col = {4}>
                      <p>{ this.props.AnneeDebut } - { this.props.AnneeFin } </p>
                  </Cell>
                
                  <Cell col = {8}>
                     <h4 style = {{ marginTop : "0px"}}>{ this.props.NomEcole } </h4>
                     <p>{ this.props.DescriptionEcole }</p>
                  </Cell>
                </Grid>
            </div>
        );
    }
}