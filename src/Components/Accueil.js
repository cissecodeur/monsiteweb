import React, { Component } from 'react';
import { Grid , Cell } from 'react-mdl';



export class Accueil extends Component {

    render(){
        return (
             <div style = {{ width :'100%', margin: "auto" }} >
               <Grid className="Accueil-grid">
                 <Cell col = {12}>
                    <img 
                    src = ""
                    alt = ""
                    className ="avatar-image img-circle"
                    /> 
                     <div className = "competences-text">
                    <h1> <marquee scrollamount= "8">Je suis M.Cisse Yacouba , je suis Administrateur Systeme , je suis Developpeur Web Full Stack,Je suis egalement un passionné de la culture DevOps... </marquee></h1>
                  
                     <h3 >Competences</h3> 
                    <hr/>
                    <p> HTML/CSS  | Bootstrap | Springboot | Reactjs/Jsx |  Wordpress | Docker | Ansible | Jenkins
                        | Grafana | Administration et Securité des Serveurs Linux
                    </p>
                    <div className = "liens-reseaux-sociaux">
                        
                       <a href = "mailto:yacoubc01@gmail.com" rel = "noopener noreferrer" target = "_blank">
                          <i className = "fa fa-envelope" aria-hidden = "true"/>
                       </a>
                                 
                                               
                       <a href = "https://gitlab.com/profile" rel = "noopener noreferrer" target = "_blank">
                          <i className = " fa fa-gitlab" aria-hidden = "true"/>
                       </a>
                     
                       <a href = "https://join.skype.com/invite/kN6TwK5g98t0" rel = "noopener noreferrer" target = "_blank">
                          <i className = "fa fa-skype" aria-hidden = "true"/>
                       </a> 

                       { /*facebook  
                       <a href = "https://github.com/cissecodeur?tab=repositories" rel = "noopener noreferrer" target = "_blank">
                          <i className = " fa fa-facebook-square" aria-hidden = "true"/>
                       </a>
                           */}
                    </div>
                 </div>
                 </Cell>
                </Grid>
             </div>
        );
    }
}

export default Accueil;