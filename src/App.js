import React from 'react';
import { Navigation, Header,Layout } from 'react-mdl';
import { NavLink } from 'react-router-dom';
import './App.css';
import { Accueil } from './Components/Accueil';
import { Projet } from './Components/Projet';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Astuces } from './Components/Astuces';


function App() {
  return (
  <BrowserRouter>
    <div className="demo-big-content">
    <Layout>
        <Header className ="header-color"  style = {{backgroundColor:"black"}}
          title= { <NavLink style = {{ textDecoration : 'none' ,color:'black'}} to ='/' >
             
          </NavLink>} scroll>
            <Navigation>
                <NavLink activeClassName="selectedLink" to ="/" style = {{ textDecoration : "none", color : "white"}}>Accueil</NavLink>
                <NavLink activeClassName="selectedLink" to ="/projets" style = {{ textDecoration : "none", color : "white"}}>Projets</NavLink>
                <NavLink activeClassName="selectedLink" to ="/astuces" style = {{ textDecoration : "none", color : "white"}}>Astuces</NavLink>
            </Navigation>
        </Header>      
    </Layout> 
</div>


       <Switch>
          <Route path = '/' component ={ Accueil } exact />
          <Route path = '/projets' component ={ Projet } />
          <Route path = '/astuces' component ={ Astuces } />
      </Switch>
        
</BrowserRouter>
       
  );
}

export default App;
